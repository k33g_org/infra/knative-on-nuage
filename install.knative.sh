#!/bin/bash
export KNATIVE_VERSION="1.0.0"
export KUBECONFIG=$PWD/config/k3s.yaml

# Install the Custom Resource Definitions (aka CRDs):

kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-crds.yaml


# Install the core components of Serving:
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-core.yaml

kubectl apply -f https://github.com/knative/net-kourier/releases/download/knative-v${KNATIVE_VERSION}/kourier.yaml

kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress-class":"kourier.ingress.networking.knative.dev"}}'


# ====== wait ... ======
kubectl wait --for=condition=Ready pod -l app=svclb-kourier -n kourier-system

kubectl wait --for=condition=available deployment/activator -n knative-serving 
kubectl wait --for=condition=available deployment/autoscaler -n knative-serving 
kubectl wait --for=condition=available deployment/controller -n knative-serving
kubectl wait --for=condition=available deployment/webhook -n knative-serving
# ======================

# Check
# Fetch the External IP address or CNAME
kubectl --namespace kourier-system get service kourier

# Configure DNS (Magic DNS xip.io)
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-default-domain.yaml


# ====== wait ... ======
kubectl wait --for=condition=complete job/default-domain -n knative-serving
# ======================

kubectl get pods --namespace knative-serving

# Knative also supports the use of the Kubernetes Horizontal Pod Autoscaler (HPA) for driving autoscaling decisions. 
# The following command will install the components needed to support HPA-class autoscaling:
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-hpa.yaml

# Install K3S and Knative on Nua.ge

## Install K3S

> Connect to the VM
```bash
ssh ubuntu@185.189.157.254
```

> Install K3s
```bash
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--tls-san 185.189.157.254" sh -s - --disable traefik
```

> Check
```bash
sudo k3s kubectl get node
```

> get the k3S.yaml file (quit the ssh session and open a terminal)
```bash
ssh ubuntu@185.189.157.254 "sudo cat /etc/rancher/k3s/k3s.yaml" > config/k3s.yaml 
```

- change `server: https://127.0.0.1:6443` by `server: https://185.189.157.254:6443`

> Check
```bash
export KUBECONFIG=$PWD/config/k3s.yaml
kubectl get nodes
```

> K9s
```bash
export KUBECONFIG=$PWD/config/k3s.yaml
k9s --all-namespaces
```

## Install Knative

```bash
export KNATIVE_VERSION="1.0.0"
export KUBECONFIG=$PWD/config/k3s.yaml

# Install the Custom Resource Definitions (aka CRDs):

kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-crds.yaml


# Install the core components of Serving:
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-core.yaml

kubectl apply -f https://github.com/knative/net-kourier/releases/download/knative-v${KNATIVE_VERSION}/kourier.yaml

kubectl patch configmap/config-network \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"ingress-class":"kourier.ingress.networking.knative.dev"}}'


# ====== wait ... ======
kubectl wait --for=condition=Ready pod -l app=svclb-kourier -n kourier-system

kubectl wait --for=condition=available deployment/activator -n knative-serving 
kubectl wait --for=condition=available deployment/autoscaler -n knative-serving 
kubectl wait --for=condition=available deployment/controller -n knative-serving
kubectl wait --for=condition=available deployment/webhook -n knative-serving
# ======================

# Check
# Fetch the External IP address or CNAME
kubectl --namespace kourier-system get service kourier

# Configure DNS (Magic DNS xip.io)
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-default-domain.yaml


# ====== wait ... ======
kubectl wait --for=condition=complete job/default-domain -n knative-serving
# ======================

kubectl get pods --namespace knative-serving

# Knative also supports the use of the Kubernetes Horizontal Pod Autoscaler (HPA) for driving autoscaling decisions. 
# The following command will install the components needed to support HPA-class autoscaling:
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v${KNATIVE_VERSION}/serving-hpa.yaml

```

## Post Knative install

Ref: https://knative.dev/docs/serving/using-a-custom-domain/

```bash
kubectl get configmap config-domain -n knative-serving -o yaml > config-map.yaml
# change IP in `192.168.1.160.sslip.io: ""` with the public IP : `185.189.157.254.sslip.io: ""`
kubectl apply -f config-map.yaml
```



#!/bin/bash
echo ${RIKER_ID_RSA_PUB} > id_rsa.pub
echo ${RIKER_ID_RSA} > id_rsa

base64 --decode --ignore-garbage id_rsa.pub > /home/gitpod/.ssh/id_rsa.pub
base64 --decode --ignore-garbage id_rsa > /home/gitpod/.ssh/id_rsa

chmod 644 /home/gitpod/.ssh/id_rsa.pub
chmod 600 /home/gitpod/.ssh/id_rsa

chmod 700 /home/gitpod/.ssh

#eval `ssh-agent`
#ssh-add
rm id_rsa
rm id_rsa.pub

